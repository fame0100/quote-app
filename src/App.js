import React, { Fragment, useState } from "react"; //Siempre importar, un fragment es como crear un div en la parte del return, pero que no se muestra en el frontend
import Header from "./componentes/Header"; //Importar el componente Header
import Formulario from "./componentes/Formulario";
import Mensaje from "./componentes/Mensaje";
import Resultado from "./componentes/Resultado";
import Spinner from "./componentes/Spinner";

function App() {
  //377. Definir el state
  const [cantidad, guardarCantidad] = useState(0); // useState retorna 2 valores, el primero es una variable que va a contener un valor y el otro es una funcion que va a estar interactuando y guardando lo que es el valor que vamos creando, al usestate se le puede dar un valor inicial por lo tanto le dimos "0"

  //377. Se cambio esta constante de Formulario.js a App.js por que se se necesita usar en otros modulos este este disponible

  //378.Leer los datos de un select
  const [plazo, guardarPlazo] = useState("");

  //383. Colocamos una nueva pieza de state
  const [total, guardarTotal] = useState(0);

  //386. Creamos un nuevo state para el spinner
  const [cargando, guardarCargando] = useState(false); //Se usa false por que cuando inicia la app no va a estar cargando

  //384. Carga condicional, para quitar el anuncio de "Mensaje"
  let componente;

  //386.Se modifica la condicion por el spinner
  if (cargando) {
    componente = <Spinner />;
  } else if (total === 0) {
    componente = <Mensaje />;
  } else {
    //386. Agregando variables a mostrar en la condicion
    componente = <Resultado total={total} plazo={plazo} cantidad={cantidad} />;
  }

  return (
    //Siempre debes de regresar un elemento que contenga los demas elementos, para eso tenemos ese div/ fragment
    <Fragment>
      <Header titulo="Cotizador de prestamos" />

      <div className="container">
        <Formulario
          cantidad={cantidad}
          guardarCantidad={guardarCantidad}
          plazo={plazo}
          guardarPlazo={guardarPlazo}
          // total={total}
          guardarTotal={guardarTotal}
          guardarCargando={guardarCargando}
        />
        {/* Le pasamos los props(destructuring) */}
        {/* Le agregamos componente a mostrar en el div */}
        <div className="mensajes">{componente}</div>
      </div>
    </Fragment>
  );

  //376. Cuando se use una clase en HTML, se tiene que usar className para que no se confunda con la clase de JS

  // <Header/> sintaxis para mandar a llamar a un componente, la ventaja de un componentes es que se puede reutilizar varias veces
}

export default App;
