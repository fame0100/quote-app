import React, { Fragment } from "react";
import App from "../App";

function Header({ titulo }) {
  //Se le puede aplicar destructuring
  //Lo que esta el return y la llave es donde se puede colocar codigo estandard de js

  return (
    <Fragment>
      <h1>{titulo}</h1>
    </Fragment>
  );
}
// h1 es html pero lo que esta dentro de las llaves interpretalo como codigo JS

//375 Diferencias de codigo ↑

/*
const Header = ({ titulo }) => (
  <Fragment>
    <h1>{titulo}</h1>
  </Fragment>
);
*/
export default Header;
