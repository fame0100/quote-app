import React, { Fragment, useState } from "react";
import { calcularTotal } from "../helpers";

const Formulario = (props) => {
  //383. Creamos una variable especial para los props, en caso de que esta sea muy larga
  const {
    cantidad,
    guardarCantidad,
    plazo,
    guardarPlazo,
    guardarTotal,
    guardarCargando,
  } = props;

  //383. Asignamos los valores al props para hacerlo mas corto

  //379. Definirr state para submit
  const [error, guardarError] = useState(false);

  //PROPS
  /*
    //377.- METODO EXPLICADO LINEA onChange lin.25
  const leerCantidad = (e) => {
    //console.log(e.target.value); //Accedemos al valor
    guardarCantidad(parseInt(e.target.value));
  };
  */

  //379. Cuando el usuario hace submit
  const calcularPrestamo = (e) => {
    e.preventDefault();
    //console.log("Enviando Formulario");

    //379. Validar el submit
    if (cantidad === 0 || plazo === "") {
      guardarError(true);
      return;
    }

    //380. Eliminar el error previo
    guardarError(false);

    //386. Habilitar el Spinner
    guardarCargando(true);

    //386. Realizamos la cotizacion despues de 3seg, por lo tanto
    setTimeout(() => {
      //
      //381. Realizar Cotizacion
      //Usamos helpers funciones que podamos llamar o utilizar
      const total = calcularTotal(cantidad, plazo);
      //console.log(total);

      //383. Guardamos el total
      guardarTotal(total);

      //386. Deshabilitamos el Spinner
      guardarCargando(false);
    }, 2000);
  };

  return (
    <Fragment>
      <form onSubmit={calcularPrestamo}>
        {/* {cantidad}
        {plazo}  */}
        <div className="row">
          <div>
            <label>Cantidad Prestamo</label>
            <input
              className="u-full-width"
              type="number"
              placeholder="Ejemplo: 3000"
              min="0"
              //   onChange={leerCantidad}
              onChange={(e) => guardarCantidad(parseInt(e.target.value))}
            />
          </div>
          <div>
            <label>Plazo para Pagar</label>
            <select
              className="u-full-width"
              onChange={(e) => guardarPlazo(parseInt(e.target.value))}
            >
              <option value="">Seleccionar</option>
              <option value="3">3 meses</option>
              <option value="6">6 meses</option>
              <option value="12">12 meses</option>
              <option value="24">24 meses</option>
            </select>
          </div>

          <div>
            <input
              type="submit"
              value="Calcular"
              className="button-primary u-full-width"
            />
          </div>
        </div>
      </form>

      {error ? (
        <p className="error">Todos los campos son obligatorios</p>
      ) : null}
      {/* Usando un operador tenario condicional */}
    </Fragment>
  );
};

export default Formulario;
